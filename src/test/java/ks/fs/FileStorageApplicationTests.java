package ks.fs;

import kn.fs.FileStorageApplication;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = FileStorageApplication.class)
class FileStorageApplicationTests {

  @Autowired private TestRestTemplate restTemplate;

  @Test
  void contextLoads() {}

  @DisplayName("Home page")
  @Test
  void homeResponse() {
    String body = restTemplate.getForObject("/", String.class);
    assertThat(body).isEqualTo("File Storage Web UI: /swagger-ui.html");
  }

  @DisplayName("Example of REST API")
  @Test
  void addResponse() {
    assertThat(restTemplate.getForObject("/add?a=2&b=3", String.class)).isEqualTo("5");
    assertThat(restTemplate.getForObject("/add?a=10&b=1243", String.class)).isEqualTo("1253");
  }
}
